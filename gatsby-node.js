const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions  
  if (node.internal.type === `PortfolioItemsJson`) {
    const slug = createFilePath({ node, getNode, basePath: `pages` })
    createNodeField({
      node,
      name: `slug`,
      value: slug,
    })
  }
}

exports.createPages = ({ graphql, actions: { createPage } }) => {
    return graphql(`
        {
            allPortfolioItemsJson {
                edges {
                    node {
                        slug
                        images
                    }
                }
            }
        }
    `).then(result => {
        result.data.allPortfolioItemsJson.edges.forEach(({ node }) => {
            createPage({
                path: node.slug,
                component: path.resolve(`./src/templates/work.js`),
                context: {
                    slug: node.slug,
                    images: node.images
                },
            })
        })
    });
  }