import React from "react"
import { graphql, Link } from "gatsby"
import Img from "gatsby-image"
import { Helmet } from "react-helmet"

const pageContent = ({ data }) => {
  return (
	<div id="page" className="hfeed site">
		<Helmet>
			<title>Project {data.portfolioItemsJson.title} - Davide Bragagnolo</title>
			<meta charSet="utf-8" />
			<meta name="robots" content="INDEX,FOLLOW"/>
			<meta name="title" content={"Project " + data.portfolioItemsJson.title + " - Davide Bragagnolo"}/>
			<meta name="description" content="a.k.a davebra, IT and code enthusiast, city rider, guitar lover and travel addict. Born and raised in Italy, based in Melbourne, Australia. I speak: TypeScript, JavaScript, NodeJS, React, PHP, Java, Swift, C#, .NET, Python, English, Italian, and so on."/>
		</Helmet>
		<div className="active">
			<div className="portfolio-single page-layout">
				<div className="layout-medium">
				<article className="hentry portfolio type-portfolio status-publish has-post-thumbnail">
					<header className="entry-header">
					<div className="portfolio-nav">
						<Link className="back internal" to="/portfolio/" title="Back to Portfolio"></Link>
					</div>
					<h1 className="entry-title">{data.portfolioItemsJson.title}</h1>
					</header>
					<div className="entry-content">
					<div className="mini-text">{data.portfolioItemsJson.description}</div>
					<h5>ABOUT THE PROJECT</h5>
					<div dangerouslySetInnerHTML={{ __html: data.portfolioItemsJson.content }} />
					</div>
					<div className="row">
					{data.allFile.edges.map( (img, i) => (
						<div key={i} className="col-md-6">
						<Img fluid={img.node.childImageSharp.fluid} />
						</div>
					))}
					</div>
					<div className="portfolio-nav bottom">
						<Link className="back internal" to="/portfolio/" title="Back to Portfolio"></Link>
					</div>
				</article>
				</div>
				<div className="layout-fixed"></div>
			</div>
		</div>
	</div>
  )
}

const query = graphql`
  query($slug: String!, $images: [String]) {
	portfolioItemsJson(slug: { eq: $slug }) {
	  title
	  description
	  content
	}
	allFile(filter: {name: {in: $images}, sourceInstanceName: {eq: "images"}}) {
	  edges {
		node {
		  childImageSharp {
			fluid(maxWidth: 600) {
				...GatsbyImageSharpFluid
			}
		  }
		}
	  }
	}
  }
`
export { query, pageContent as default };