import React from "react"
import { graphql, StaticQuery } from 'gatsby'
import Img from "gatsby-image"

const MapMarker = () => (
    <StaticQuery query={graphql`
        query {
            marker: file(relativePath: { eq: "images/marker.png" }) {
                childImageSharp {
                    fixed(width: 108) {
                        ...GatsbyImageSharpFixed
                    }
                }
            }
        }
  `}
    render = { data => (
        <Img fixed={data.marker.childImageSharp.fixed} style={{transform: 'translate(-50%, -100%)'}} />
    )} 
    />
)

export default MapMarker