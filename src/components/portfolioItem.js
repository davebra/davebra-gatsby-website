import React from "react"
import { Link } from "gatsby"
import Img from "gatsby-image"

export default class PortfolioItem extends React.Component {
    render() {
      return (
        <div className={`media-cell hentry portfolio ${this.props.data.node.category}`}>
            <div className="media-box">
                <Img fluid={this.props.image.node.childImageSharp.fluid} />
                <div className="mask"></div>
                <Link className="internal" to={`/${this.props.data.node.slug}`}>&nbsp;</Link>
            </div>
            <div className="media-cell-desc">
                <h3>{this.props.data.node.title}</h3>
                <p className="category">{this.props.data.node.description}</p>
            </div>
        </div>
        );
    }
}
