import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"
import Img from "gatsby-image"

class HeaderSidebar extends React.Component {
    render() {
        return (
            <header className="header" role="banner">	
                <span className="menu-toggle toggle-link" aria-hidden="true">&nbsp;</span>
                <h1 className="site-title mobile-title">@DAVEBRA</h1>
                <div className="header-wrap">
                    <Img fixed={this.props.icon.davebra.childImageSharp.fluid} style={{width: '100%'}} fadeIn={false} alt="davebra" imgStyle={{position:'static'}} />
                    <h1 className="site-title">@DAVEBRA</h1>
                    <nav id="primary-navigation" className="site-navigation primary-navigation" role="navigation">
                        <div className="nav-menu">
                            <ul>
                            <li className="current_page_item">
                                <Link to="/"><i className="pe-7s-home"></i>Home</Link>
                            </li>
                            <li>
                                <Link to="/about/"><i className="pe-7s-user"></i>About Me</Link>
                            </li>
                            <li>
                                <Link to="/resume/"><i className="pe-7s-id"></i>Resume</Link>
                            </li>
                            <li>
                                <Link to="/portfolio/"><i className="pe-7s-glasses"></i>Portfolio</Link>
                            </li>
                            <li>
                                <Link to="/contact/"><i className="pe-7s-call"></i>Contact</Link>
                            </li>
                            </ul>
                        </div>
                    </nav>
                    <div className="header-bottom"> 
                        <ul className="social">
                            <li><a target="_blank" className="github" href="https://github.com/davebra" rel="noopener noreferrer">&nbsp;</a></li>
                            <li><a target="_blank" className="instagram" href="https://www.instagram.com/davebra/" rel="noopener noreferrer">&nbsp;</a></li>
                            <li><a target="_blank" className="linkedin" href="http://au.linkedin.com/in/davidebragagnolo/" rel="noopener noreferrer">&nbsp;</a></li>
                            <li><a target="_blank" className="stack-overflow" href="http://stackoverflow.com/users/6512376/davebra" rel="noopener noreferrer">&nbsp;</a></li>
                        </ul>                       
                        <div className="copy-text">
                            <p>&copy; 2024 Davebra</p>
                        </div>  
                    </div>
                </div>
            </header>
            )
    }
}

const headerContent = () => (
    <StaticQuery
      query={graphql`
        query {
            davebra: file(relativePath: { eq: "images/davebra300.png" }) {
                childImageSharp {
                    fluid(quality: 90, maxWidth: 300) {
                        ...GatsbyImageSharpFluid_withWebp_noBase64
                    }
                }
            }
        }
      `}
      render={(data) => (
        <HeaderSidebar icon={data} />
      )}
    />
)

export default headerContent;