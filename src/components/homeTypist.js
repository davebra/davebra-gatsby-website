import React from 'react';
import Typist from 'react-typist';
 
export default class HomeTypist extends React.Component {
    state = {
        typing: true,
    }
    done = () => {
        this.setState({ typing: false }, () => {
            this.setState({ typing: true })
        });
    }
    render() {
        return this.state.typing ? 
        <Typist cursor={{show: false}} onTypingDone={this.done}>
            <span>&nbsp;SOFTWARE ENGINEER.</span>
            <Typist.Backspace count={21} delay={3000} />
            <span>&nbsp;CITY RIDER.</span>
            <Typist.Backspace count={21} delay={3000} />
            <span>&nbsp;FULL-STACK DEVELOPER.</span>
            <Typist.Backspace count={21} delay={3000} />
            <span>&nbsp;CODE & IT ENTHUSIAST.</span>
            <Typist.Backspace count={11} delay={3000} />
            <span>&nbsp;GUITAR LOVER.</span>
            <Typist.Backspace count={13} delay={3000} />
        </Typist>
        : ''
    }
}