import React from "react"
import HeaderSidebar from '../components/headerSidebar'

const pageContent = () => { return (
	<div id="page" className="hfeed site">
		<HeaderSidebar />
		<div id="main" className="site-main" style={{backgroundColor: '#0108fa', color: '#fff'}}>
		<section>
			<div className="content">
				<div className="layout-medium">
					<h1 className="page-title">Oooops... </h1>
					<div className="row">
						<div className="col-md-8 col-md-offset-2">
							<p style={{textAlign: 'center'}}>Sorry mate, this page doesn't exists.</p>
						</div>
					</div>
					
				</div>
			</div>
		</section>
		</div>
		<div className="p-overlay"></div>
		<div className="site-alert animated"></div>
	</div>
)}

export default pageContent;