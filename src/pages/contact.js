import React from "react"
import HeaderSidebar from '../components/headerSidebar'
import MapMarker from '../components/mapMarker'
import GoogleMapReact from 'google-map-react'
import { Helmet } from "react-helmet"

const pageContent = () => { return (
	<div id="page" className="hfeed site">
		<Helmet>
			<title>Contact, socialize - Davide Bragagnolo</title>
			<meta charSet="utf-8" />
			<meta name="robots" content="INDEX,FOLLOW"/>
			<meta name="title" content="Contact, socialize - Davide Bragagnolo"/>
			<meta name="keywords" content="software, engineer, full-stack, developer, programmer, apps, cloud, architect, melbourne, davebra, davide, bragagnolo"/>
			<meta name="description" content="a.k.a davebra, IT and code enthusiast, city rider, guitar lover and travel addict. Born and raised in Italy, based in Melbourne, Australia. I speak: TypeScript, JavaScript, NodeJS, React, PHP, Java, Swift, C#, .NET, Python, English, Italian, and so on."/>
		</Helmet>
		<HeaderSidebar />
		<div id="main" className="site-main">
			<section id="contact" className="pt-page page-current page-layout"    
				style={{
					backgroundColor: '#E5DCC5'
				}}>
				<div className="content">
					<div className="layout-medium">
						<h1 className="page-title">
							<i className="pe-7s-mail-open-file"></i>
							contact </h1>
						
						<div className="row">
							<ul className="social">
								<li><a target="_blank" className="mail" href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;%68%65%6C%6C%6F%40%64%61%76%65%62%72%61%2E%6D%65" rel="noopener noreferrer">&nbsp;</a></li>
								<li><a target="_blank" className="github" href="https://github.com/davebra" rel="noopener noreferrer">&nbsp;</a></li>
								<li><a target="_blank" className="instagram" href="https://www.instagram.com/davebra/" rel="noopener noreferrer">&nbsp;</a></li>
								<li><a target="_blank" className="linkedin" href="http://au.linkedin.com/in/davidebragagnolo/" rel="noopener noreferrer">&nbsp;</a></li>
								<li><a target="_blank" className="stack-overflow" href="http://stackoverflow.com/users/6512376/davebra" rel="noopener noreferrer">&nbsp;</a></li>
								<li><a target="_blank" className="behance" href="http://www.behance.net/davebra" rel="noopener noreferrer">&nbsp;</a></li>
								<li><a target="_blank" className="facebook" href="https://www.facebook.com/davide.bragagnolo" rel="noopener noreferrer">&nbsp;</a></li>
								<li><a target="_blank" className="skype" href="skype:davidebragagnolo?chat" rel="noopener noreferrer">&nbsp;</a></li>
								<li><a target="_blank" className="twitter" href="https://twitter.com/davebra" rel="noopener noreferrer">&nbsp;</a></li>
							</ul>
						</div>
						<div className="row">
							<div className="col-sm-12">
								<p>&nbsp;</p>
							</div>
						</div>
						<div className="map" style={{ height: '100vh', width: '100%' }}>
							<GoogleMapReact
								bootstrapURLKeys={{key: "AIzaSyBdLid2TPO_JZIcNuJUIo9XKpYfr1UWlrg"}}
								defaultCenter={{ lat: -37.813628, lng: 144.963058 }}
								defaultZoom={5}
								>
								<MapMarker lat={-37.813628} lng={144.963058} />
							</GoogleMapReact>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div className="p-overlay"></div>
		<div className="site-alert animated"></div>
	</div>
)}

export default pageContent;