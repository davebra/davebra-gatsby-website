import React from "react"
import { graphql, StaticQuery } from 'gatsby'
import Masonry from 'react-masonry-component';
import HeaderSidebar from '../components/headerSidebar'
import PortfolioItem from "../components/portfolioItem"
import { Helmet } from "react-helmet"

const pageContent = () => { return (
	<div id="page" className="hfeed site">
        <Helmet>
            <title>Portfolio, works, projects - Davide Bragagnolo</title>
            <meta charSet="utf-8" />
            <meta name="robots" content="INDEX,FOLLOW"/>
            <meta name="title" content="Portfolio, works, projects - Davide Bragagnolo"/>
            <meta name="keywords" content="software, engineer, full-stack, developer, programmer, apps, cloud, architect, melbourne, davebra, davide, bragagnolo"/>
            <meta name="description" content="a.k.a davebra, IT and code enthusiast, city rider, guitar lover and travel addict. Born and raised in Italy, based in Melbourne, Australia. I speak: TypeScript, JavaScript, NodeJS, React, PHP, Java, Swift, C#, .NET, Python, English, Italian, and so on."/>
        </Helmet>
		<HeaderSidebar />
		<div id="main" className="site-main">
        <StaticQuery query={graphql`
            query {
                allPortfolioItemsJson {
                    edges {
                        node {
                            title
                            slug
                            description
                            category
                            thumbnail
                        }
                    }
                }
                allFile(filter: {name: {regex: "/-thumb/"}, sourceInstanceName: {eq: "images"}}) {
                    edges {
                        node {
                            name
                            childImageSharp {
                                fluid(maxWidth: 500) {
                                    ...GatsbyImageSharpFluid_withWebp_noBase64
                                }
                            }
                        }
                    }
                }
            }
        `}
            onFilter = {(d) => {
                console.log(d);
            }}
            render = { data => (
                <section id="portfolio" className="pt-page page-layout portfolio"
                    style={{
                        backgroundColor: '#60d7a9'
                    }}>
                    <div className="content">
                        <div className="layout-medium">
                            <h1 className="page-title">
                                <i className="pe-7s-folder"></i>
                                portfolio </h1>
                            <Masonry
                                className={'portfolio-items media-grid'}
                                id={'portfolio-masonry'}
                                options={{
                                    transitionDuration: 0
                                }}
                                disableImagesLoaded={false}
                                updateOnEachImageLoad={false}
                                
                            >
                                {data.allPortfolioItemsJson.edges.map( (node, i) => (
                                        <PortfolioItem key={i} data={node} image={data.allFile.edges.find(n => { return n.node.name === node.node.thumbnail })} />
                                    )
                                )}
                            </Masonry>
                        </div>
                    </div>
                </section>
            )} />
		</div>
	</div>
)}

export default pageContent;