import React from "react"
import HeaderSidebar from '../components/headerSidebar'
import { Helmet } from "react-helmet"

const pageContent = () => { return (
	<div id="page" className="hfeed site">
		<Helmet>
			<title>About me, who I am - Davide Bragagnolo</title>
			<meta charSet="utf-8" />
			<meta name="robots" content="INDEX,FOLLOW"/>
			<meta name="title" content="About me, who I am - Davide Bragagnolo"/>
			<meta name="keywords" content="software, engineer, senior, lead, full-stack, developer, programmer, apps, cloud, architect, melbourne, davebra, davide, bragagnolo"/>
			<meta name="description" content="a.k.a davebra, IT and code enthusiast, city rider, guitar lover and travel addict. Born and raised in Italy, based in Melbourne, Australia. I speak: TypeScript, JavaScript, NodeJS, React, PHP, Java, Swift, C#, .NET, Python, English, Italian, and so on."/>
		</Helmet>
		<HeaderSidebar />
		<div id="main" className="site-main">
			<section id="about" className="pt-page page-current page-layout">
				<div className="content">
					<div className="layout-medium">
						<h1 className="page-title">
							<i className="pe-7s-glasses"></i>
							about me </h1>
						<div className="row">
							<div className="col-md-8 col-md-offset-2">
								<p><strong>Disclaimer: no AIs were harmed in the making of this website, it's all human made.</strong></p>
								<p>I was born and grow up between two of most beautiful cities of Italy, <a href="https://en.wikipedia.org/wiki/Padua" target="_blank" rel="noopener noreferrer">Padova</a> and <a href="https://en.wikipedia.org/wiki/Venice" target="_blank" rel="noopener noreferrer">Venezia</a>, and I started coding as a hobby, in my spare time during the high school.</p>
								<p>As my interest in coding was growing, I started to create small apps and websites, I was actually more interested on what was behind the scene. I hold a Degree in New Media and a Bachelor of Information Technology with Valedictorian Award, but I consider myself a self-taught engineer, most of the languages and technologies I know, I learned browsing, reading,  trying and making mistakes. The hard way.</p>
								<p>In 2010 I work as freelance, I develop apps and websites for companies, agencies, startups and other freelancers. Some of my clients and collaborators are <a href="https://www.mediagraflab.it/" target="_blank" rel="noopener noreferrer">Mediagraf Lab (Padova, Italy)</a>, <a href="https://maiarellistudio.com/" target="_blank" rel="noopener noreferrer">Maiarelli Studio (New York, USA)</a>, <a href="https://www.yourmurano.com" target="_blank" rel="noopener noreferrer">VeniceCommerce (Venezia, Italy)</a>, Tankboys (Venezia, Italy), <a href="http://www.lankydesign.com/" target="_blank" rel="noopener noreferrer">Lanky Design (Padova, Italy)</a>.</p>
								<p>In 2011, I became a teacher. I taught programming languages to students of vocational courses and high school. Here some of the schools I worked: <a href="https://www.issm.it/" target="_blank" rel="noopener noreferrer">Istituto Salesiano San Marco (Venezia, Italy)</a>, <a href="https://www.iisvalle.edu.it" target="_blank" rel="noopener noreferrer">IIS Giovanni Valle (Padova, Italy)</a>, <a href="https://www.canossiane-treviso.it" target="_blank" rel="noopener noreferrer">Istituto Canossiano Madonna del Grappa (Treviso, Italy)</a>, <a href="https://lepidorocco.it" target="_blank" rel="noopener noreferrer">Training Center of Professional of Lancenigo(Treviso, Italy)</a>.</p>
								<p>In September 2015 I moved to Australia and traveled around this big country with a self-built campervan, one of the craziest and best experiences of my life. After about one year and more than 35 thousands of kilometers, I stopped in Melbourne, which I currently live in.</p>
								<p>Today, I work as software engineer @ <a href="https://www.papercut.com/" target="_blank" rel="noopener noreferrer">PaperCut</a>, a fast-growing international software company based in Melbourne, Australia.</p>
								<p>Music lover, guitarist (&#8230;rarely), traveler and probably a nerd. Could be weirdo too :) I&#8217;m always learning and trying new things, now I&#8217;m deep into React Native, Vue.js, Python and Serverless architectures, knowledge of IT must be kept constantly up-to-date, and I&#8217;ve some nice projects in my mind.</p>
								<p><strong>What technologies do I know?</strong> Well, it's a long list: TypeScript, JavaScript, NodeJS, React, Angular, TypeScript, Go, PHP, C#, Python, SQL, NoSQL, GraphQL, HTML, CSS, SCSS, LESS, C++, ASP, .NET, Java, Swift, Objective-C, GIT, XML, YAML, Markdown, Regex, Unity... <br />Doesn&#8217;t make sense to continue this list I guess, certainly is more important to say that I can work on all architectures (Serverless, OOP, MVC, Progressive Web Applications, etc) in any kind of environment, everything else is just syntax and APIs.</p>
								<p><strong>Preferred IDE?</strong> Depends on the project actually, I find myself confortable with <em>Visual Studio Code</em>, but on my list there are also <em>Xcode</em> and <em>Android Studio</em>, they are still necessary for some projects. <em>Docker</em> as development environment, or <em>AWS / Google Cloud / Azure</em> if I need more power and for production environments. Oh yeah, never forget to git stash/commit/push everything!!!</p>
								<p><strong>Tabs vs Spaces?</strong> I&#8217;m on <em><a href="https://silicon-valley.fandom.com/wiki/Richard_Hendricks" target="_blank" rel="noopener noreferrer">Richard Hendricks</a></em> side: tabs! But, with the IDE setup to output Spaces or Tabs on situation. Both wins!</p>
								<p><strong>Mac vs Windows?</strong> Why nobody never ask Linux&#8230; Mac's still my favourite, virtual machines and Cloud services are the solutions for making all environments happy!</p>
							</div>
						</div>
						<div className="section-title center">
							<h2><i>Fun Numbers</i></h2>
						</div>
						<div className="row">
							<div className="col-sm-3 col-xs-6">
								<div className="fun-fact">
									<p><i className="pe-7s-map-2"></i></p>
									<h4>21: countries and special regions visited, only missing 212</h4>
								</div>
							</div>
							<div className="col-sm-3 col-xs-6">
								<div className="fun-fact">
									<p><i className="pe-7s-bicycle"></i></p>
									<h4>10: total wheels of my vehicles</h4>
								</div>
							</div>
							<div className="col-sm-3 col-xs-6">
								<div className="fun-fact">
									<p><i className="pe-7s-global"></i></p>
									<h4>zero: servers I own. Love serverless!</h4>
								</div>
							</div>
							<div className="col-sm-3 col-xs-6">
								<div className="fun-fact">
									<p><i className="pe-7s-car"></i></p>
									<h4>35,463km: longest road-trip...on 4 wheels :)</h4>
								</div>
							</div>
						</div>
						<div className="section-title center">
							<h2><i>So smooth and fast</i></h2>
						</div>
						<div className="row">
							<div className="col-md-8 col-md-offset-2">
								<p>Oh, if you like this website, and you&#8217;re asking yourself how it can be so smooth and fast... Well, it&#8217;s based on a ThemeForest&#8217;s theme (with licence, of course) optimized, built on top of React with GatsbyJS, and hosted on Netlify. But I'm sure you already know all of that, right? (thanks Wappalyzer) What you probably don't know, is that I didn't use ChatGPT or other AIs, so I hope you don't mind some typos.</p>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div className="p-overlay"></div>
		<div className="site-alert animated"></div>
	</div>
)}

export default pageContent;