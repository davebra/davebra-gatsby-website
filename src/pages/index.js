import React from "react"
import { graphql, StaticQuery } from "gatsby"
import HeaderSidebar from '../components/headerSidebar'
import BackgroundImage from 'gatsby-background-image'
import HomeTypist from "../components/homeTypist"
import { Helmet } from "react-helmet"

const pageContent = () => (
	<StaticQuery
		query={graphql`
			query {
				desktop: file(relativePath: { eq: "images/home_bg_davebra.jpg" }) {
					childImageSharp {
						fluid(quality: 100, maxWidth: 1920) {
						...GatsbyImageSharpFluid_withWebp_noBase64
						}
					}
				}
			}
		`}
		render={ data => (
			<div id="page" className="hfeed site">
				<Helmet>
					<title>Davide Bragagnolo - Software Engineer & Full-Stack Developer</title>
					<meta charSet="utf-8" />
					<meta name="robots" content="INDEX,FOLLOW"/>
					<meta name="title" content="Davide Bragagnolo - Software Engineer & Full-Stack Developer"/>
					<meta name="keywords" content="software, engineer, full-stack, developer, programmer, apps, cloud, architect, melbourne, davebra, davide, bragagnolo"/>
					<meta name="description" content="a.k.a davebra, IT and code enthusiast, city rider, guitar lover and travel addict. Born and raised in Italy, based in Melbourne, Australia. I speak: TypeScript, JavaScript, NodeJS, React, PHP, Java, Swift, C#, .NET, Python, English, Italian, and so on."/>
					<meta property="og:image" content="https://davebra.me/images/davebra/davebra1024.png" />
					<meta property="og:locale" content="en_US" />
					<meta property="og:type" content="website" />
					<meta property="og:title" content="Davide Bragagnolo, a.k.a. davebra &#8211; Full-Stack Developer" />
					<meta property="og:description" content="a.k.a davebra, IT and code enthusiast, city rider, guitar lover and travel addict. Born and raised in Italy, based in Melbourne, Australia. I speak: TypeScript, JavaScript, NodeJS, React, PHP, Java, Swift, C#, .NET, Python, English, Italian, and so on." />
					<meta property="og:url" content="https://davebra.me" />
					<meta property="og:site_name" content="Davide Bragagnolo" />
					<meta name="twitter:card" content="summary" />
					<meta name="twitter:site" content="@davebra" />
					<meta name="twitter:creator" content="@davebra" />
					<meta name="twitter:title" content="Davide Bragagnolo, a.k.a. davebra &#8211; Full-Stack Developer" />
					<meta name="twitter:description" content="a.k.a davebra, IT and code enthusiast, city rider, guitar lover and travel addict. Born and raised in Italy, based in Melbourne, Australia. I speak: TypeScript, JavaScript, NodeJS, React, PHP, Java, Swift, C#, .NET, Python, English, Italian, and so on." />
					<meta name="twitter:image" content="https://davebra.me/images/davebra/davebra1024.png" />
				</Helmet>
				<HeaderSidebar />
				<div id="main" className="site-main">
					<BackgroundImage Tag="section"
									id="home"
									className="pt-page page-current page-layout light-text home-section has-bg-img"
									fluid={data.desktop.childImageSharp.fluid}>
						<div className="content">
							<div className="layout-medium">
							<h1 style={{
								fontSize: '1.60em'
							}}>
								DAVIDE BRAGAGNOLO
							</h1>
							<h4>
							<HomeTypist />
							</h4>
							</div>
						</div>
					</BackgroundImage>
				</div>
				<div className="p-overlay"></div>
				<div className="site-alert animated"></div>
			</div>
		)}
	/>
)

export default pageContent;