import React from "react"
import { graphql, StaticQuery, Link } from "gatsby"
import HeaderSidebar from '../components/headerSidebar'
// import Img from "gatsby-image"
import { Helmet } from "react-helmet"

const pageContent = () => (
	<StaticQuery
	  query={graphql`
		query {
			giancarlo: file(relativePath: { eq: "images/ref_giancarlos.jpg" }) {
				childImageSharp {
					fixed(width: 72, height: 72) {
					...GatsbyImageSharpFixed_withWebp_noBase64
					}
				}
			}
			giona: file(relativePath: { eq: "images/ref_gionam.jpg" }) {
				childImageSharp {
					fixed(width: 72, height: 72) {
					...GatsbyImageSharpFixed_withWebp_noBase64
					}
				}
			}
		}
	  `}
	  render={ data => (
		<div id="page" className="hfeed site">
			<Helmet>
				<title>Resume, curriculum, skills, career - Davide Bragagnolo</title>
				<meta charSet="utf-8" />
				<meta name="robots" content="INDEX,FOLLOW"/>
				<meta name="title" content="Resume, curriculum, skills, career - Davide Bragagnolo"/>
				<meta name="keywords" content="software, engineer, senior, lead, full-stack, developer, programmer, apps, cloud, architect, melbourne, davebra, davide, bragagnolo"/>
				<meta name="description" content="a.k.a davebra, IT and code enthusiast, city rider, guitar lover and travel addict. Born and raised in Italy, based in Melbourne, Australia. I speak: TypeScript, JavaScript, NodeJS, React, PHP, Java, Swift, C#, .NET, Python, English, Italian, and so on."/>
			</Helmet>
			<HeaderSidebar />
			<div id="main" className="site-main">
				<section id="resume" className="pt-page page-current page-layout" 
					style={{
						backgroundColor: '#EBF0DF'
					}}>
					<div className="content">
						<div className="layout-medium">
							<h1 className="page-title">
								<i className="pe-7s-news-paper"></i>
								resume </h1>
							<div className="row">
								<div className="col-sm-7">
									<div className="event">
										<h2>WORK HISTORY</h2>
										<p><i className="pe-7s-ribbon"></i></p>
									</div>
									<div className="event">
										<h3>Jan 2022 &#8211; Present</h3>
										<h4>Senior Software Engineer</h4>
										<h5>@ PaperCut &#8211; Melbourne, Australia</h5>
										<p>PaperCut is a fast-growing international software company, I'm working as software engineer on their existing and exciting new projects, with particular focus on automations and integrations between systems.
										</p>
									</div>
									<div className="event">
										<h3>Aug 2017 &#8211;Dec 2021</h3>
										<h4>Full-Stack Developer</h4>
										<h5>@ Crumpler &amp; Tigerlily &#8211; Melbourne, Australia</h5>
										<p>With the agencies Aquent (Aug 2017 - Oct 2019) and Wall Street HR (Nov 2019 - Dec 2021), I worked for Crumpler & Tigerlily, two iconic Australian brands who design and make bags and swimwear, where I was busy with development of integrations, automations and the e-commerce stacks.
										</p>
									</div>
									<div className="event">
										<h3>Dec 2016 &#8211; Jul 2017</h3>
										<h4>Senior Software Developer</h4>
										<h5>@ NoahCode Pty Ltd &#8211; Melbourne, Australia</h5>
										<p>NoahCode was an after school coding program company, I developed a NodeJS-MongoDB-AngularJS web application for code learning and a PHP-MySQL CRM for the company, other tasks included the creation of curriculums (study plan, exercises, activities, lessons) for students of primary and secondary schools, tutoring for teachers, management of the AWS infrastructure. NoahCode has been sold during 2017.
										</p>
									</div>
									<div className="event">
										<h3>Dec 2011 &#8211; Jul 2015</h3>
										<h4>Teacher of programming</h4>
										<h5>@ Istituto Salesiano San Marco &#8211; Venezia, Italy</h5>
										<p>I&#8217;ve taught programming languages to professional/vocational courses and high school's students, courses included the teaching of Back-End development with PHP-MySQL, Front-End with HTML-CSS-JavaScript, and CMS, such as Magento, WordPress, Joomla and Drupal.
										</p>
									</div>
									<div className="event">
										<h3>Jul 2010 &#8211; Dec 2015</h3>
										<h4>Freelance Full-Stack and Software Developer</h4>
										<h5>@ Myself &#8211; Various Places</h5>
										<div className="page" title="Page 2">
											<div className="layoutArea">
												<div className="column">
													<p>I have developed apps, web applications and websites for companies and agencies, some of the companies and agencies with whom I had the pleasure to work are:<br />
														&#8211; <a href="http://www.mediagrafspa.it/" target="_blank" rel="noopener noreferrer">Mediagraf S.p.A.</a>, editorial and printing company &#8211; Padova, Italy<br />
														&#8211; <a href="http://www.mediagraflab.it/" target="_blank" rel="noopener noreferrer">Mediagrf LAB</a>, digital department of by Mediagraf S.p.A. &#8211; Padova, Italy<br />
														&#8211; <a href="http://maiarellistudio.com/" target="_blank" rel="noopener noreferrer">Maiarelli Studio</a>, graphic and design agency &#8211; New York, USA<br />
														&#8211; <a href="http://www.lankydesign.com/" target="_blank" rel="noopener noreferrer">Lanky Design</a>, graphic and design agency &#8211; Padova, Italy<br />
														&#8211; <a href="http://venicecommerce.com/" target="_blank" rel="noopener noreferrer">Venicecommerce</a>, web/ecommerce agency &#8211; Venezia, Italy<br />
														&#8211; <a href="http://www.tankboys.biz/" target="_blank" rel="noopener noreferrer">Tankboys</a>, graphic and design agency &#8211; Venezia, Italy</p>
												</div>
											</div>
										</div>
									</div>
									<div className="event">
										<h2>EDUCATION</h2>
										<p><i className="pe-7s-study"></i></p>
									</div>
									<div className="event">
										<h4>Developer Programmer</h4>
										<h5>@ Self-taught</h5>
										<div className="page" title="Page 1">
											<div className="layoutArea">
												<div className="column">
													<p>IT is a world constantly changing, there's always something new to learn. On top of that, I&#8217;m a nerd, so I&#8217;m curious to try and understand new things.</p>
												</div>
											</div>
										</div>
									</div>
									<div className="event">
										<h4>Bachelor of Information Technology</h4>
										<h5>@ Academy of Information Technology – Melbourne, Australia</h5>
										<div className="page" title="Page 1">
											<div className="layoutArea">
												<div className="column">
													<p>Bachelor obtained with Valedictorian Award. Yes, I had to do the speech... Final year project: <Link className="internal" to={`/work/iha`}>Indoor Home Automation</Link></p>
												</div>
											</div>
										</div>
									</div>
									<div className="event">
										<h4>Mobile apps  &amp; web apps designer &amp; developer</h4>
										<h5>@ Istituto Salesiano San Marco &#8211; Venezia, Italy</h5>
										<div className="page" title="Page 3">
											<div className="layoutArea">
												<div className="column">
													<p>Diploma course in New Media, degree obtained with the highest score. Final year project: <Link className="internal" to={`/work/digital-mag-kiosk-app`}>Digital Mag Kiosk App</Link></p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div className="col-sm-5">
									<div className="section-title center">
										<h2><i>Coding Skills</i></h2>
									</div>
									<div className="skill-unit">
										<h4>TypeScript</h4>
										<div className="bar" data-percent="90">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>JavaScript &amp; NodeJS</h4>
										<div className="bar" data-percent="90">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>Golang</h4>
										<div className="bar" data-percent="70">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>PHP</h4>
										<div className="bar" data-percent="95">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>React &amp; React Native</h4>
										<div className="bar" data-percent="80">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>C#, .NET &amp; ASP</h4>
										<div className="bar" data-percent="80">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>DevOps servers, serverless &amp; cloud</h4>
										<div className="bar" data-percent="90">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>Terraform</h4>
										<div className="bar" data-percent="60">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>HTML5 + CSS3 (Less &amp; Sass)</h4>
										<div className="bar" data-percent="95">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>Java</h4>
										<div className="bar" data-percent="80">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>Python</h4>
										<div className="bar" data-percent="70">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>Swift</h4>
										<div className="bar" data-percent="60">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>C++</h4>
										<div className="bar" data-percent="70">
											<div className="progress"></div>
										</div>
									</div>
									<div className="section-title center">
										<h2><i>Engineering Skills</i></h2>
									</div>
									<div className="skill-unit">
										<h4>Problem Solving</h4>
										<div className="bar" data-percent="95">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>Multitasking</h4>
										<div className="bar" data-percent="90">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>Product ownership &amp; decision make</h4>
										<div className="bar" data-percent="85">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>Teamwork</h4>
										<div className="bar" data-percent="99">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>Team leadership</h4>
										<div className="bar" data-percent="90">
											<div className="progress"></div>
										</div>
									</div>
									<div className="section-title center">
										<h2><i>Other Skills</i></h2>
									</div>
									<div className="skill-unit">
										<h4>European Football</h4>
										<div className="bar" data-percent="90">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>Pizza make</h4>
										<div className="bar" data-percent="95">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>Pasta Carbonara</h4>
										<div className="bar" data-percent="99">
											<div className="progress"></div>
										</div>
									</div>
									<div className="skill-unit">
										<h4>Guitar</h4>
										<div className="bar" data-percent="55">
											<div className="progress"></div>
										</div>
									</div>
									{/* <div className="section-title center">
										<h2><i>Referees</i></h2>
									</div>
									<div className="testo">
										<p style={{textAlign: 'center'}}>
											<Img Tag="a" placeholderClassName="alignnone size-full" fixed={data.giona.childImageSharp.fixed} style={{overflow: 'visible', margin: '-60px 0 10px'}}  />
										</p>
										<h4>Giona Maiarelli</h4>
										<h5>CEO / Maiarelli Studio</h5>
										<p>Besides being extremely skilled and highly motivated Davide is also a pleasure to work with.
										</p>
									</div>
									<div className="testo">
										<p style={{textAlign: 'center'}}>
											<Img Tag="a" placeholderClassName="alignnone size-full" fixed={data.giancarlo.childImageSharp.fixed} style={{overflow: 'visible', margin: '-60px 0 10px'}} />
										</p>
										<h4>Giancarlo Salvador</h4>
										<h5>CEO, Art Director / Lanky Design</h5>
										<p>It’s a very professional and precise programmer with many solutions and suggestions coming up in situations of critical issues as many times could happen on web projects. You can count on him especially on stressful moments.
										</p>
									</div> */}
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div className="p-overlay"></div>
			<div className="site-alert animated"></div>
		</div>
	  )}
	/>
)

export default pageContent;