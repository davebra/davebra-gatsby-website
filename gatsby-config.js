const path = require(`path`)

module.exports = {
    siteMetadata: {
      title: `Davide Bragagnolo - Software Engineer & Full-Stack Developer`,
      siteUrl: `https://davebra.me`,
      description: `a.k.a davebra, IT and code enthusiast, city rider, guitar lover and travel addict. Born and raised in Italy, based in Melbourne, Australia. I speak: TypeScript, JavaScript, NodeJS, React, PHP, Java, Swift, C#, .NET, Python, English, Italian, and so on.`,
    },
    plugins: [
      {
        resolve: `gatsby-source-filesystem`,
        options: {
          name: `images`,
          path: path.join(__dirname, `src`, `data`),
        },
      },
      {
        resolve: `gatsby-transformer-json`,
        options: {
          typename: `Json`,
        },
      },
      {
        resolve: 'gatsby-plugin-robots-txt',
        options: {
          host: 'https://davebra.me',
          sitemap: 'https://davebra.me/sitemap-index.xml',
          policy: [{ userAgent: '*', allow: '/' }]
        }
      },
      {
        resolve: `gatsby-plugin-google-analytics`,
        options: {
          // The property ID; the tracking code won't be generated without it
          trackingId: "UA-98446894-1",
          // Defines where to place the tracking script - `true` in the head and `false` in the body
          head: false,
          // Setting this parameter is optional
          anonymize: true,
          // Setting this parameter is also optional
          respectDNT: true,
          // Avoids sending pageview hits from custom paths
          //exclude: ["/preview/**", "/do-not-track/me/too/"],
          // Delays sending pageview hits on route update (in milliseconds)
          pageTransitionDelay: 50,
          // Enables Google Optimize using your container Id
          //optimizeId: "YOUR_GOOGLE_OPTIMIZE_TRACKING_ID",
          // Enables Google Optimize Experiment ID
          //experimentId: "YOUR_GOOGLE_EXPERIMENT_ID",
          // Set Variation ID. 0 for original 1,2,3....
          //variationId: "YOUR_GOOGLE_OPTIMIZE_VARIATION_ID",
          // Defers execution of google analytics script after page load
          defer: true,
          // Any additional optional fields
          //sampleRate: 5,
          //siteSpeedSampleRate: 10,
          //cookieDomain: "example.com",
          enableWebVitalsTracking: true,
        },
      },
      `gatsby-plugin-sitemap`,
      `gatsby-plugin-react-helmet`,
      `gatsby-plugin-catch-links`, 
      `gatsby-transformer-sharp`, 
      `gatsby-plugin-sharp`
    ]
  }